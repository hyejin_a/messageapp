Rails.application.routes.draw do
  resources :message_threads do
    resources :messages
  end
  root 'message_threads#index'

end
