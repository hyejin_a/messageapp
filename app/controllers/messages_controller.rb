class MessagesController < ApplicationController
  before_action :set_message_thread

  def index
    @messages = @message_thread.messages
    @message = @message_thread.messages.new
  end

  def new
    @message = @message_thread.messages.new
  end

  def create
    @message = @message_thread.messages.new(message_params)
    if @message.save
      redirect_to message_thread_messages_path(@message_thread)
    end
  end

private
  def message_params
    params.require(:message).permit(:body, :user_id)
  end

  def set_message_thread
  	@message_thread = MessageThread.find(params[:message_thread_id])
  end
end
