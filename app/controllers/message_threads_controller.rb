class MessageThreadsController < ApplicationController
	def index
		@users = User.all
		@message_threads = MessageThread.all
	end

	def create
   if MessageThread.between(params[:sender_id],params[:recipient_id]).present?
      @message_thread = MessageThread.between(params[:sender_id], params[:recipient_id]).first
    else
      @message_thread = MessageThread.create!(message_thread_params)
    end
    redirect_to message_thread_messages_path(@message_thread)
	end

private
	def message_thread_params
		params.permit(:sender_id, :recipient_id)
	end
end
