class Message < ActiveRecord::Base
  belongs_to :message_thread
  belongs_to :user

  validates_presence_of :body, :message_thread_id, :user_id
end
