class User < ActiveRecord::Base
  has_many :message_threads, :foreign_key => :sender_id
end
